from scipy.io import wavfile
import speech_recognition as sr
from playsound import playsound
from os import getcwd
from pydub.playback import play
from pydub import AudioSegment



# fs, data = wavfile.read(filename)



audio_path2 = f"{getcwd()}/static/audio"

def listen2():
    speech = None
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Say something!")
        audio = r.listen(source)

    play(AudioSegment.from_mp3(f"{audio_path2}/clearly.wav"))

    # recognize speech using Google Speech Recognition
    try:
        # for testing purposes, we're just using the default API key
        # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        # instead of `r.recognize_google(audio)`
        speech = r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")
        print(f"Google Speech Recognized: '{speech}'")
        return speech
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
        return f"Error happened {e}"
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
        return f"Error happened {e}"
    except Exception as e:
        return f"Error happened {e}"


listen2()
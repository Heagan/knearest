from draw import *
from knearest_c import Brain
from random import randint

from time import time

'''
5.38
3.65
'''

def main():
    speed = time()

    w, h = (400, 400)
    init("XOR", (w, h))
    update()

    brain = Brain(2, K=10)

    xor = {
        # (0.5, 0.5): 0, 
        (1, 1): 0,
        (0, 0): 0,
        (0, 1): 1, 
        (1, 0): 1,
    }
    # xor = {
    #     (0.5, 0.5): 0, 
    #     (1, 1): 1,
    #     (1, 0.5): 1,
    #     (0, 0.5): 1,
    #     (0, 0): 1,
    #     (0, 1): 1,
    #     (0.5, 0): 1,
    #     (0.5, 1): 1, 
    #     (1, 0): 1,
    # }
    # xor = {
    #     (1, 7): 1,
    #     (1, 12): 1,
    #     (2, 7): 1,
    #     (2, 9): 1,
    #     (2, 11): 1,
    #     (3, 6): 1,
    #     (3, 10): 1,
    #     (3.5, 8): 1,
    #     (2.5, 9): 0,
    #     (3.5, 3): 0,
    #     (5, 3): 0,
    #     (6, 1): 0,
    #     (3, 2): 0,
    #     (4, 2): 0,
    #     (5.5, 4): 0,
    #     (7, 2): 0,
    # }





    print("Training Brain")
    def train():
        for x in xor:
            brain.add(x, xor[x])
        # i = randint(0, len(xor) - 1)
        # j = 0
        # for x in xor:
        #     if j == i:
        #         brain.add(x, xor[x])
        #     j += 1
            
    def points():
        resolution = 10
        cols = int(w / resolution)
        rows = int(h / resolution)
        for d in brain.points:
            x = int( (cols * d.inputs[0] * resolution) - (resolution / 2) )
            y = int( (rows * d.inputs[1] * resolution) - (resolution / 2) )
            rect(x, y, resolution, resolution, (255, 0, 0))

    def calc():
        resolution = 10
        cols = int(w / resolution) # 40
        rows = int(h / resolution) # 40
        for i in range(0, cols):
            for j in range(0, rows):
                x1 = i / cols # (0 / 40) = 0 - 1
                x2 = j / rows # (0 / 40) = 0 - 1

                # pred = brain.best([x1, x2])[0]
                pred = brain.predict([x1, x2])
                # print(pred)
                y = pred * 155

                # print(pred)
                c = int(min(255, y))
                rect(i * resolution, j * resolution , resolution, resolution, (c, c, c))
        points()

    for t in range(0, 500):
        train()

    calc()
    # print("Completed Calculation")
    # rect(50, 50, w - 100, h - 100)
    update()
    # breakpoint()


    # print("Loading a simple model")
    
    # for x in range(w):
    #     for y in range(h):
    #         best = brain.best((x / w, y / h))
    #         c = int(min(255, best[0] * 255))
    #         putpixel(x, y, (c, c, c))
    # update()
    # print("Loading a more complicated model")
    # for x in range(w):
    #     for y in range(h):
    #         best = brain.best((x / w, y / h))
    #         brain.add((x / w, y / h), best[0])
    #         c = int(min(255, best[1] * 255))
    #         putpixel(x, y, (c, c, c))
    print("Time taken:", round(time() - speed, 2) )
    while True:
        update()

if __name__ == "__main__":
    main()


# brain.add([0.5, 0.5], 0) ; calc() ; update()












